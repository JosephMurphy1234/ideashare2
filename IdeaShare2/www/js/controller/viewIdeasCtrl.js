﻿var app = angular.module('starter');

app.controller('viewIdeasController',
    ['$state', '$scope', '$http', 'mockItemService', function ($state, $scope, $http, mockItemService) {

        //$rootScope.hideit = true;

        //Loads and displays all ideas
        $scope.ideas = [];

        $scope.loading = 1;
        $http.get("http://ideashareapi.azurewebsites.net/api/Ideashare/GetAllIdeas").success(function (data) {


            for (var i = 0; i < data.length; i++) {

                $scope.ideas.push({

                    id: data[i].Id, subBy: data[i].UserId, title: data[i].IdeaTitle, content: data[i].IdeaTitle

                });

            }
            $scope.loading = 0;
        })

            .error(function (error, status) {
                $scope.data.error = { message: error, status: status };
                console.log($scope.data.error.status);
            });

    $scope.selectIdea = function (t) {

        var result = { id: t };

        $state.go('editIdea', result);
    };

}]);