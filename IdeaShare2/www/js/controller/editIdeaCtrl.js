﻿
var app = angular.module('starter');

app.controller('EditIdeaController',
    ['$state', '$scope', '$http', '$window', '$stateParams', 'mockItemService', '$cordovaImagePicker', '$cordovaCamera', '$cordovaToast',
        function ($state, $scope, $http, $window, $stateParams, mockItemService, $cordovaImagePicker, $cordovaCamera, $cordovaToast) {
            //debugger;
    $scope.loading = 0;

    $scope.id = $stateParams.id;
    $scope.tab = 0;
    document.getElementById("tab0").style.backgroundColor = 'gray';
    $scope.idea = {};
    $scope.idea.id = $scope.id || 0;
    $scope.image = { url: '', name:'' };
    $scope.document = { url: '', name: '' };
    $scope.showImageName = false;
    $scope.Keywords;
    $scope.Comment;
    $scope.Status;

    $scope.documents = mockItemService.getDocuments();
    //$scope.sites = mockItemService.getSites();
    //$scope.productTypes = mockItemService.getProductTypes();
    //$scope.processTypes = mockItemService.getProcessTypes();
    //$scope.drivers = mockItemService.getBusinessDrivers();

            //var idea = mockItemService.getIdeaById($scope.id);
            
    if ($scope.id != null)
    {
        //Gets data for particular item
        $scope.loading = 1;
        $http.get("http://ideashareapi.azurewebsites.net/api/Ideashare/GetSingleIdea", {
            params: { id: $scope.id }
        }).success(function (data) {

            $scope.idea = {

                id: $scope.id, subBy: data[0].UserId, title: data[0].IdeaTitle, ideaContent: data[0].IdeaContent, Keywords: data[0].Keywords, Comment: data[0].Comment
               
            };
        })

        .error(function (data) {
            alert(data);
        });
        $scope.loading = 0;
    }
    //else
    //{
    //    $scope.idea.drivers = JSON.parse(JSON.stringify($scope.drivers));
    //}
   
    //Unused, app used to use mockItemService
    $scope.images = mockItemService.getImagesById($scope.id);
    $scope.documents = mockItemService.getDocumentsById($scope.id);
    $scope.links = mockItemService.getLinksById($scope.id);

    $scope.file = [];
    $scope.img = [];
    $scope.link = [];


    //if (idea) {
    //    $scope.idea = JSON.parse(JSON.stringify(idea));
    //}
    //else {
    //    $scope.idea.drivers = JSON.parse(JSON.stringify($scope.drivers));
    //}

    //Shows if tab is selected
    $scope.selectTab = function (val) {

        $scope.tab = val;

        $scope.tabList = [0, 1];

        for (var i = 0; i < $scope.tabList.length; i++) {
            if ($scope.tab == $scope.tabList[i]) {
                document.getElementById("tab" + i).style.backgroundColor = 'gray';
                //$scope.test = tab;
            }
            else {
                document.getElementById("tab" + i).style.backgroundColor = '#F1F1F1';
            }
        }

    };

    $scope.cancel = function () {
        $state.go('home');
    };

    //Saves item to database and returns to the Home screen. Media upload functionality will also have to be added.
    $scope.saveAll = function () {
        //$scope.saveNewIdea();
        //$scope.uploadImage();
        //$scope.uploadDocument();
        //$scope.saveLink();
        $scope.pushItem();
        $window.history.back();
        //bsLoadingOverlayService.start();
    }

    //Saves item to the database.
    $scope.pushItem = function () {

        //$scope.testIdea = {

        //    id: 10, UserId: "Me", title: "Test", IdeaContent: "Test"

        //};

        var testObject = new Object();
        $scope.idea.UserId = 1;
        //$scope.idea.Site = "Test";
        testObject.Idea = $scope.idea;
      
        var url = 'http://ideashareapi.azurewebsites.net/api/Ideashare/CreateIdea';

        if ($scope.id != null)
        {
            url = 'http://ideashareapi.azurewebsites.net/api/Ideashare/EditIdea'
        }

        $http.post(url, testObject).success(function (data, status, headers, config) {


            $scope.message = data;
            //alert($scope.message);
        }).error(function (data, status, headers, config) {
            alert("failure message: " + JSON.stringify({ data: data }));
        });
    }

    //$scope.uploadDocument = function () {
    //    //for (var i = 0; i < $scope.file.length; i++) {
    //    //    var doc = { id: $scope.id, document: $scope.file[i], createdBy: 'Me', created: new Date() };
    //    //    mockItemService.uploadDocument(doc);
    //    //}
    //    //$scope.documents = mockItemService.getDocumentsById($scope.id);
    //    $scope.uploadFile();
    //};


    //Saves locally stored document - not fully implemented.
    $scope.uploadDocument = function () {
        var fileInput = document.getElementById('testUpload');
        var file = fileInput.files[0];

        //var fileName = file.name;
        
            var reader = new FileReader();

            reader.onload = function(e) {
                reader.result = reader.result;
                var result = reader.result;
                var x = String.fromCharCode.apply(null, new Uint8Array(result));

                //$scope.playAudio(result);

                //console.log("This: " + x);

                //console.log("This: " + new Int8Array(e.target.result));
            }
        
            if (file == undefined)
            {
                return;
            }

            reader.readAsArrayBuffer(file);
            console.log(reader.result);
        
        
        
    };

    //Test function
    $scope.playAudio = function (audioData) {
        var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
        var source;
        
        source = audioCtx.createBufferSource();

        audioCtx.decodeAudioData(audioData, function(buffer) {
            source.buffer = buffer;

            source.connect(audioCtx.destination);
            source.loop = true;
            source.start(0);
        },

      function(e){ console.log("Error with decoding audio data" + e.err); });

    }

    //Saves link - not fully implemented
    $scope.saveLink = function () {

        var link = { id: $scope.id, title: $scope.link.title, url: $scope.link.url };
        if ($scope.link.title == undefined)
        {
            //$scope.showToast('Please type in a URL', 'short', 'bottom');
            return;
        }
        mockItemService.saveLink(link);
        $scope.links = mockItemService.getLinksById($scope.id);
    };

    $scope.showToast = function (message, duration, location) {

        $cordovaToast.show(message, duration, location)
            .then(function (success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });

    };

    //$scope.saveNewIdea = function () {
    //    $scope.idea.subBy = 'Me';
    //    $scope.id = mockItemService.saveIdea($scope.idea);
    //    $scope.showToast('Saved Item', 'short', 'bottom');
    //};

    //Saves idea for approval to database
    $scope.saveIdeaForApproval = function () {
        $scope.idea.Status = 'Pending';
        //$scope.id = mockItemService.saveIdea($scope.idea);
        //$scope.showToast('Sent Item for Approval', 'short', 'bottom');
        $scope.pushItem();
        $window.history.back();
    };

    //Gets saved image from internal storage
    $scope.getImage = function () {

        var options = {
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        };

        //if (device.platform === "Android") {
        //    setInterval(function () {
        //        cordova.exec(null, null, '', '', [])
        //    }, 200);
            
        //}

        $cordovaCamera.getPicture(options)
            .then(function (imageData) {
                
                if (device.platform === "Android") {
                   
                    //$scope.test(imageData);

                    $scope.image.url = imageData;
                    
                    $scope.showImageName = true;
                }
        //        else
        //        {
        //            $scope.image.uri = imageData;
        //            $scope.image.name = imageDate.split('/').slice(-1).pop();
        //        }
        //    }, function (error) {
        //        // error getting photos
        //        console.log(error);
            });

    };

    //$scope.test = function (data) {
    //    window.FilePath.resolveNativePath(data,
    //                     function (nativeImage) {

    //                         //$scope.image.url = nativeImage;
    //                         //$scope.image.name = nativeImage.split('/').slice(-1).pop();

    //                         //$scope.showImageName = true;
    //                     },
    //                     function (err) {
    //                         console.log(nativeUri);
    //                     });
    //};

    $scope.displayImage = function (image) {
        var queryString = { img: image };
        $state.go('displayImage', queryString);
    };


    //Uploads image - not fully implemented
    $scope.uploadImage = function () {
        //debugger;
        var image = { id: $scope.id, url: $scope.image.url, name: $scope.image.name, createdBy: 'Me', created: new Date() };
        if ($scope.image.url == "") {
            //$scope.showToast('Please upload an image', 'short', 'bottom');
            return;
        }
        if (image.id) {
            mockItemService.uploadImage(image);
            $scope.image.url = "";
            $scope.images = mockItemService.getImagesById($scope.id);
            $scope.image = { url: '', name: '' };
            $scope.showImageName = false;
        }
        else {
            $scope.showToast('Save idea before adding image', 'long', 'bottom');
        }

    };

    //Gets locally stored document path
    $scope.getDocument = function () {

            fileChooser.open(function (uri) {
                window.FilePath.resolveNativePath(uri,
                    function (nativeUri) {
                        $scope.document.url = nativeUri;
                        $scope.document.name = nativeUri.split('/').slice(-1).pop();
                    },
                    function (err) {
                        console.log(nativeUri);
                    });
            });

    };

    $scope.futureFeature = function () {
        $scope.showToast('This feature will be included in a future release', 'short', 'bottom');
    }

}]);
