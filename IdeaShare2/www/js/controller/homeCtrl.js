﻿var app = angular.module('starter');

app.controller('homeCtrl',
    ['$state', '$scope', '$http', 'mockItemService',
        function ($state, $scope, $http, mockItemService) {
            //$scope.ideas = mockItemService.getIdeas();
           

            //Populates data from database
            $scope.ideas = [];
          
            $scope.loading = 1;

            $scope.img_source = "../www/img/Logo.jpg";

            $http.get("http://ideashareapi.azurewebsites.net/api/Ideashare/GetAllIdeas").success(function (data) {


                for (var i = 0; i < data.length; i++) {

                    $scope.ideas.push({

                        id: data[i].Id, subBy: data[i].UserId, title: data[i].IdeaTitle, content: data[i].IdeaTitle, site: data[i].Site, status:data[i].Status

                    });
                }

            })

                .error(function (error, status) {
                    $scope.data.error = { message: error, status: status };
                    console.log($scope.data.error.status);
                });

            $scope.loading = 0;

            $scope.viewAll = function () {
                $state.go('allIdeas');
            };

            $scope.newIdea = function () {
                $state.go('editIdea');
            };

            $scope.selectIdea = function (t) {

                var result = { id: t };
                $state.go('editIdea', result);
            };

            //Not sure if this is needed? It may have been a test function that is now used when the page loads above.
            //$scope.getList = function () {
               
            //    $http.get("http://ideashareapi.azurewebsites.net/api/Ideashare/GetAllIdeas").success(function (data) {
            //        debugger;

            //        for (var i = 0; i < data.length; i++) {

            //            $scope.ideas.push({

            //                id: i, subBy: data[i].UserId, title: data[i].IdeaTitle, content: data[i].IdeaTitle

            //            });

            //            var y = String(data[0].Taglist);

            //        }

            //    })

            //    .error(function (error, status) {
            //        $scope.data.error = { message: error, status: status };
            //        console.log($scope.data.error.status);
            //    });

            //    var z = 4;

            //}
            
}]);