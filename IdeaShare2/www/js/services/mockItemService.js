﻿var app = angular.module('starter');

app.service('mockItemService', ['$sessionStorage', function ($sessionStorage) {

    //Loading the Storage Components
    if (!$sessionStorage.ideas) {
        $sessionStorage.ideas = [{

            id: 1, subBy: 'Me', title: 'idea 1', content: 'Pending', keywords: ['Test'], processType: 'Plates', productType: 'Trauma',
            site: 'Blackpool', approver: 'Stephen', instantisNumber: '100', costBenefit: '450', status: 'Pending',
            drivers: { Cost: true, Growth: true, Quality: false, Service: true },
            Comment: 'Testing Comments'

        },
             {

                 id: 2, subBy: 'Stephen', title: 'idea 2', content: 'Pending', keywords: ['Keyword 2'], processType: 'Assembly', productType: 'Hips',
                 site: 'Cork', approver: 'Stephen', instantisNumber: '100', costBenefit: '450', status: 'Approved',
                 drivers: { Cost: true, Growth: true, Quality: false, Service: true },
                 Comment: 'Testing Comments 2'
             }
        ];
    }

    if(!sessionStorage.sites){
        $sessionStorage.sites = ['Balsthal', 'Bettlach', 'Blackpool',
        'Brandywine', 'Cork', 'Elmira', 'Grenchen', 'Haegendork',
        'Le Locle', 'Leeds', 'Mezzovico', 'Monument', 'Oberdorf',
        'Palm Beach Gardens', 'Raron', 'Raynham', 'Suzhou',
        'Swiss Campus', 'Tuttligen', 'Umkirch', 'Warsaw'];

    }

    if (!sessionStorage.processTypes) {
        $sessionStorage.processTypes = ['Anodisation', 'Assembly', 'Cleaning',
            'Coating', 'Electropolishing', 'Energy', 'Forming',
        'Inspection', 'Liquid Fill', 'Logistics', 'Machining', 'Material',
        'Mill & Blend', 'Packaging', 'Passivation', 'Planning', 'Plates',
        'Powder Fill', 'Procurement', 'Sterilization', 'Surface Treatment'];
    }

    if (!sessionStorage.productTypes) {
        $sessionStorage.productTypes = ['Cements & Accessories', 'CMF', 'Customer Devices'
        , 'Extremities', 'Hips', 'Hydrocephaly', 'Instruments', 'Knees',
        'Nails', 'Not Product Related', 'Plates', 'Poly', 'Power Tools',
        'Screws', 'Shoulders', 'Spine', 'Trauma'];
    }

    if (!sessionStorage.businessDrivers) {
        $sessionStorage.businessDrivers = { Quality: false, Service: false, Growth: false, Cost: false };
    }

    if (!sessionStorage.images) {
        $sessionStorage.images = [];
    }

    if(!sessionStorage.documents){
        $sessionStorage.documents = [];
    }

    if(!sessionStorage.links){
        $sessionStorage.links = [];
    }

    //########## Functions ###########
    this.getIdeas = function () {

        return $sessionStorage.ideas;
    };

    this.getIdeaById = function (id) {

        var ideas = $sessionStorage.ideas.filter(function (obj) {
            return obj.id === id;
        });

        return ideas[0];
    };

    this.getSites = function () {
        return $sessionStorage.sites;
    };

    this.getProcessTypes = function () {
        return $sessionStorage.processTypes;
    };

    this.getProductTypes = function () {
        return $sessionStorage.productTypes;
    };

    this.getBusinessDrivers = function () {
        return $sessionStorage.businessDrivers;
    };

    this.getImages = function () {
        return $sessionStorage.images;
    };

    this.getImagesById = function (id) {

        var img = [];

        for (var i = 0; i < $sessionStorage.images.length; i++) {
            if ($sessionStorage.images[i].id === id) {
                img.push($sessionStorage.images[i]);
            }
        }

        return img;

    };

    this.uploadImage = function (img) {
        $sessionStorage.images.push(img);
    };

    this.getDocuments = function () {
        return $sessionStorage.documents;
    };

    this.getDocumentsById = function (id) {
        var docs = [];

        for (var i = 0; i < $sessionStorage.documents.length; i++) {
            if ($sessionStorage.documents[i].id === id) {
                docs.push($sessionStorage.documents[i]);
            }
        }

        return docs;
    };

    this.uploadDocument = function (doc) {
        $sessionStorage.documents.push(doc);
    };

    this.getLinksById = function (id) {
        var l = [];

        for (var i = 0; i < $sessionStorage.links.length; i++) {
            if ($sessionStorage.links[i].id === id) {
                l.push($sessionStorage.links[i]);
            }
        }

        return l;
    };

    this.saveLink = function (link) {
        $sessionStorage.links.push(link);
    };

    this.saveIdea = function (idea) {

        var index = -1;
        var id;
        for (var i = 0; i < $sessionStorage.ideas.length; i++) {
            if ($sessionStorage.ideas[i].id === idea.id) {
                index = i;
                break;
            }
        }

        if (index > -1) {
            $sessionStorage.ideas[index] = idea;
            id = idea.id;
        }
        else {
            var maxVal = 0;
            for (var j = 0; j < $sessionStorage.ideas.length; j++) {
                if ($sessionStorage.ideas[j].id > maxVal)
                    maxVal = $sessionStorage.ideas[j].id;
            }

            idea.id = maxVal + 1;
            id = idea.id;
            $sessionStorage.ideas.push(idea);
        }

        return id;
    };
}]);