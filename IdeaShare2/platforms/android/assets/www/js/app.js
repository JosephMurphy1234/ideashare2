// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngStorage', 'ngCordova'])

.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        if (cordova.platformId === "ios" && window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });

})

.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
            .state('home', {
                cache: false,
                url: '/home',
                templateUrl: 'templates/home.html',
                controller: 'homeCtrl'
            })

    .state('allIdeas', {
        cache: false,
        url: '/allIdeas',
        templateUrl: 'templates/allIdeas.html',
        controller: 'viewIdeasController'
    })
            .state('editIdea', {
                url: '/editIdea',
                templateUrl: 'templates/editIdea.html',
                controller: 'EditIdeaController',
                params: {
                    'id': null
                }
            })
            .state('displayImage', {
                url: '/displayImage',
                templateUrl: 'templates/displayImage.html',
                controller: 'DisplayImageController',
                params: {
                    'img': {}
                }
            });

    $urlRouterProvider.otherwise('/home');

    //$urlrouteProvider.when('/editIdea', {
    //    templateUrl: 'templates/editIdea.html',
    //    controller: 'EditIdeaCtrl',
    //    resolve: {
    //        books: function (srvLibrary) {
    //            return srvLibrary.getBooks();
    //        }
    //    }
    //});

});
